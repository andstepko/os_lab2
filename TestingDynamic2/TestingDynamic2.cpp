// TestingDynamic2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <stdio.h>
#include <ctime>


#include "Windows.h"

using namespace std;

typedef int (__stdcall *GENKEY)(int&, int&);
typedef int (__stdcall *CRYPT)(int, int, int);

int _tmain(int argc, _TCHAR* argv[])
{	
	HMODULE h;
	h = LoadLibrary(_T("Lab2dynamic.dll"));
	GENKEY genkey = (GENKEY)GetProcAddress(h, "_genkey@8");
	CRYPT crypt = (CRYPT)GetProcAddress(h, "_crypt@12");
	int e, d, n;
	bool b = true;
	if(h)
	{
		n = genkey(e, d);
		//n = genkey(7, 11);
		for(int a = 2; a < 20; a++)
		{
			int a1 = crypt(a, e, n);
			int a2 = crypt(a1, d, n);
			if(a != a2)
			{
				b = false;
				break;
			}
		}
	}
	else
	{

	}
	if(b)
		printf("%s\n", "OK");
	else
		printf("%s\n", "ERROR");
	return 0;
}
