#include "stdafx.h"
#define STATIC
#include "RSA.h"
#include <stdlib.h>
#include <ctime>
//using namespace std;

int numberOfBits(int input)
{
	int result = 0;
	while(input != 0)
	{
		input = input / 2;
		result++;
	}
	return result;
}
int MathMult(int a, int b)
{
	return (a * b);
}
int MathSqr(int input)
{
	return MathMult(input, input);
}
int MathMod(int input, int module)
{
	return input % module;
}
int myMathPow(int input, int power, int module)
{
	int result = input;
	int count = numberOfBits(power) - 2;
	for(int i = count; i >= 0; i--)
	{
		result = MathMod(MathSqr(result), module);
		if(power & (1 << i))
			result = MathMod(MathMult(result, input), module);
	}
	return result;
}

int simple(int min, int max)
{
	srand( time(0) );
	int r = rand() % max + min;
	if(r % 2 == 0)
		r++;
	bool b = true;
	while(b)
	{
		for(int i = 3; i*i <= r; i += 2)
		{
			if(r % i == 0)
			{
				b = false;
				break;
			}
		}
		if(!b)
		{
			r += 2;
			b = true;
		}
		else 
			break;
	}
	return r;
}
int simple()
{
	return simple(11, 30);
}

int gcd(int a, int b)
{
	while((a != 0) && (b != 0))
	{
		if (a > b)
			a = a % b;
		else
			b = b % a;
	}
	return (a + b);
}

RSA_API int  __stdcall genkey(int &e, int &d)
{
	int p = simple();
	/*int q = simple();
	while(q == p)
		q = simple();*/
	int q;
	do {q = simple();}
	while(q == p);
	int n = MathMult(p, q);
	int ph = (p - 1) * (q - 1);
	for(e = 3; gcd(e, ph) != 1; e += 2);
	for(d = 3; d * e % ph != 1; d++);
	return n;
}

RSA_API int __stdcall crypt(int input, int key, int n)
{
	return myMathPow(input, key, n);
}