#pragma once

#ifdef STATIC
#define RSA_API
#else
#ifdef DYNAMIC
#define RSA_API __declspec(dllexport)
#else 
#define RSA_API __declspec(dllimport)
#endif
#endif

#ifdef __cplusplus
extern "C"
{
#endif

RSA_API int __stdcall genkey(int& e, int& d);
RSA_API int __stdcall crypt(int a, int key, int n);

#ifdef __cplusplus
}
#endif