// testingDynamic.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "..\Lab2\RSA.h"
#include <stdio.h>

int _tmain(int argc, _TCHAR* argv[])
{
	int e, d, n;
	n = genkey(e, d);
	bool b = true;
	for(int a = 2; a < 20; a++)
	{
		int a1 = crypt(a, e, n);
		int a2 = crypt(a1, d, n);
		if(a != a2)
		{
			b = false;
			break;
		}
	}
	if(b)
		printf("%s\n", "OK");
	else
		printf("%s\n", "ERROR");
	return 0;
}

