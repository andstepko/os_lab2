// ResourceDLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "ResourceDLL.h"


// This is an example of an exported variable
RESOURCEDLL_API int nResourceDLL=0;

// This is an example of an exported function.
RESOURCEDLL_API int fnResourceDLL(void)
{
	return 42;
}

// This is the constructor of a class that has been exported.
// see ResourceDLL.h for the class definition
CResourceDLL::CResourceDLL()
{
	return;
}
