// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the RESOURCEDLL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// RESOURCEDLL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef RESOURCEDLL_EXPORTS
#define RESOURCEDLL_API __declspec(dllexport)
#else
#define RESOURCEDLL_API __declspec(dllimport)
#endif

// This class is exported from the ResourceDLL.dll
class RESOURCEDLL_API CResourceDLL {
public:
	CResourceDLL(void);
	// TODO: add your methods here.
};

extern RESOURCEDLL_API int nResourceDLL;

RESOURCEDLL_API int fnResourceDLL(void);
